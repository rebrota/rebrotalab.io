---
title: Ficha Técnica
layout: default
---

<div class="container-fluid">
  <div class="row">
<div class="ficha-tecnica col-sm-10">
  <h1>Ficha Técnica</h1>
  <p><b>Organização e curadoria:</b> Artur Cabral e Luciany Osório</p>
  
  <p><b>Assistentes de produção técnica e executiva:</b> Equipe MediaLab/UNB (Nycácia Florindo e Tainá Luize)</p>

  <p><b><a href="mailto:medialabunbdf@gmail.com">Fale com a gente ;)</a></b></p>
  <p>
    <b>Artistas:</b><br>
    <span>
      Ana Lúcia Canetti<br>
      Ana Moravi<br>
      Balduino <br>
      Bárbara de Azevedo<br>
      Bryan Carvalho<br>
      euFraktus_X<br>
      Fabrício Dias<br>
      Fau Martins<br>
      FEBA<br>
      Felipe Braga<br>
      Flavia Fabiana <br>
      Gabriel Oliveira <br>
      Giselle Dias<br>
      Guilherme Albuquerque<br>
      Guilherme Campos<br> 
      Gustavo Bays<br>
      joseMar blures<br>
      Jurema<br>
      Levi Tapuia<br>
      Lucca Toschi<br>
      Luciana Arslan e Paulo Augusto - coletivo Lupa<br>
      Lynn Carone<br>
      Manu Romeiro<br>
      Mari Gemma De La Cruz<br>
      Marta Mencarini<br>
      Merielin Gomes <br>
      Michelle Duarte<br>
      Patrícia Siqueira<br>
      Philipe Kaoká <br>
      Pirá Arte<br>
      Robson Castro<br>
      Ryan Hermogenio<br>
      Triz de Oliveira Paiva<br>
      Veronica Lindquist<br>
      Viníciu Fagundes - Estúdio Tutóia
    </span>
  </p>
</div>
</div>
</div>


