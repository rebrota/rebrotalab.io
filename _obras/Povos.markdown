---
layout: obra
title: Povos do Cerrado
thumbnail: /assets/thumbnail/t-Povos.png
artista: Flavia Fabiana 
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;flaviafabiana_arte
t-bio: Flavia Fabiana &#233; artista visual e arte educadora, natural de Anápolis – GO. Reside e trabalha em sua cidade natal e tamb&#233;m em Goiânia – GO. Mulher branca cis, de cinquenta anos, de origem humilde. Se formou na FAV&#47;UFG &#40;Faculdade de Artes Visuais&#47;Universidade Federal de Goiás&#41;. Ao terminar a faculdade se dedicou a licenciatura em arte. Em 2015 retoma uma pesquisa há muito esquecida no campo acad&#234;mico. No ano de 2019 realiza sua primeira exposi&#231;&#227;o individual, em Anápolis. Desde ent&#227;o, segue os percursos direcionados por seus estudos. Seu trabalho art&#237;stico narra as rela&#231;&#245;es com a memória, ancestralidade, o tempo, o corpo e tamb&#233;m seus afetos. Usa como principal material de estudo, o cabelo humano. Mais que fios, mechas, cores, texturas e volumes, considera o cabelo, uma potencial paisagem orgânica, enquanto representa pessoas e as retrata em toda sua subjetividade e importância. Nas mais variadas experimenta&#231;&#245;es, t&#233;cnicas, suportes e m&#237;dias, por meio de coletas e inventariados desta materialidade, se expressa atrav&#233;s do desenho, pintura, fotografia, performances entre outros. Provocando reflex&#245;es em meio aos conceitos e ao uso subjetivo ou direto, desta materialidade preciosa repleta de simbolismos. Participa de exposi&#231;&#245;es desde 2015.


texto-descricao: Pesquisar sobre o cabelo humano me fez refletir sobre muitas de suas nuances, simbolismos, significados e desdobramentos.  Vejo o quando esta materialidade se relaciona com a ancestralidade, memória, o afeto e o corpo. Mais que fios, mechas, cores, texturas e volumes, o cabelo, se torna uma potencial paisagem orgânica, enquanto representa pessoas e as retrata em toda sua subjetividade, simplismo e importância.  E nos desdobramentos destes estudos, iniciei a s&#233;rie onde fotografo com o celular as pessoas de costas. Ao retratar uma pessoa nesta posi&#231;&#227;o, busco ampliar o conceito de retrato, n&#227;o só o rosto nos representa. Mas, nossos cabelos trazem muito de nossa ancestralidade, individualidade e identidade. Por&#233;m, esta amplitude reflete tamb&#233;m para o conceito de paisagem. Ao falar em cerrado, pensa&#45;se muito em sua paisagem &#40;fauna, flora, suas cores e texturas&#41; exuberante. O que &#233; um fato. Mas para mim, as pessoas tamb&#233;m fazem parte e complementam esta paisagem, os habitantes deste habitat t&#227;o fantástico devem se complementar e estar em harmonia.  Por isso, apresento este pol&#237;ptico de fotografias de pessoas que habitam o cerrado, intitulado “Povos do Cerrado”.  
ano: 2023
tecnica: fotografia 
---

  <img src="/assets/obras/Povos/1.png" alt="Povos do Cerrado" class="img-fluid d-block">
  