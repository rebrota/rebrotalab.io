---
layout: obra
title: O guardião do Cerrado , O beijo d'água , M&#227;e Natureza 
thumbnail: /assets/thumbnail/t-O.png
artista: Gabriel Oliveira 
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;gabriel.oliveirart 
t-bio: Nasci e me criei e meio ao Cerrado tocantinense, desde de que me entendo por gente já apreciava observar tudo que era vivo, com um olhar especial e a partir dessa observa&#231;&#227;o e carinho pela natureza comecei a ilustrar já muito novo, hoje retrato o Cerrado com t&#233;cnicas como pontilhismo e hachuras, sou acad&#234;mico de Engenharia Florestal pela Universidade Federal do Tocantins o que agrega mais valor cient&#237;fico à minhas obras. 
ano: 2022
tecnica: Desenho 
---
  
  <img src="/assets/obras/O/1.jpeg" alt="Mãe Natureza" class="img-fluid d-block">
  `Mãe Natureza`<br>  <br>
  <img src="/assets/obras/O/2.jpeg" alt="O beijo d'água" class="img-fluid d-block">
  `O beijo d'água`<br><br>
  <img src="/assets/obras/O/3.jpeg" alt="O guardião do Cerrado" class="img-fluid d-block">
  `O guardião do Cerrado`<br><br>

