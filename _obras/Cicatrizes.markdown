---
layout: obra
title: Cicatrizes
thumbnail: /assets/thumbnail/t-Cicatrizes.png
artista: Giselle Dias
l-bio: https&#58;&#47;&#47;www.canva.com&#47;design&#47;DAFregwb6lc&#47;5awfUFGPg64_t1F7RPxN0A&#47;edit?utm_content=DAFregwb6lc&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton
t-bio: Artista t&#234;xtil que investiga as possibilidades do bordado nas visualidades contemporâneas , arte educadora na Rrede Federal de Ensino e mestranda do Programa de Pós&#45;gradua&#231;&#227;o ao PROF&#45;ARTES.
texto-descricao: Sobre imagens recentes veiculadas em mat&#233;rias jornal&#237;sticas  que denunciam o aumento do desmatamento do cerrado no ultimo ano, impressas sobre tecido algod&#227;o cru foram realizadas investidas em bordado com o ponto chamado cicatriz. A po&#233;tica do ponto bordado dialoga com a doloroza realidade da a&#231;&#227;o desumuna de desmate em um bioma t&#227;o fundamental para a manuten&#231;&#227;o do equil&#237;brio hidrológico no pa&#237;s. 
ano: 2023
tecnica: Bordado sobre impress&#227;o digital
---
<div class="menu d-none d-sm-block">


  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
     


   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/cicatrizes/1.png" alt="Cicatrizes" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/cicatrizes/2.png" alt="Cicatrizes" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/cicatrizes/3.png" alt="Cicatrizes" class="img-fluid mx-auto d-block">
      </div>
      
  </div>
 
 
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>





<div class="d-block d-sm-none">
  
  <img src="/assets/obras/cicatrizes/1.png" alt="Cicatrizes" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/cicatrizes/2.png" alt="Cicatrizes" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/cicatrizes/3.png" alt="Cicatrizes" class="img-fluid" width="35%">
  
</div>

