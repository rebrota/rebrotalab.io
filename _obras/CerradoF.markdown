---
layout: obra
title: CerradoF&#234;nix_Dan&#231;a_Ninhos
thumbnail: /assets/thumbnail/t-CerradoF.png
artista: Lynn Carone
l-bio: https&#58;&#47;&#47;lynncarone.com&#47;
t-bio: Lynn Carone Natural de Los Angeles, EUA, formou&#45;se como artista na FAAP, SP, 1988. Atualmente vive em Bras&#237;lia, lecionou Artes e &#233; mestre em Arte e tecnologia na Universidade de Bras&#237;lia &#40;UnB&#41;. Sua pesquisa art&#237;stica envolve práticas de “site specific”, fotografia, v&#237;deo instala&#231;&#245;es, objeto e gravura.Fizeram parte de sua forma&#231;&#227;o artistas renomados como Carlos Fajardo, Edith Derdik, Nuno Ramos, Carmela Gross, Evandro Carlos Jardim, Suzette Venturelli, Denise Camargo. Participou de exposi&#231;&#245;es internacionais como “Fish eye”&#47; Cardiff, o seminário Internacional de arte e Natureza&#47;USP, Panoramas, Val&#234;ncia&#47;Espanha e Link2021 art&design em Auckland&#47;New Zeland, IX Cinabeh, e de diversas exposi&#231;&#245;es coletivas e individuais no Brasil como Pa&#231;o das Artes&#47;SP, MAC de Curitiba e de Campinas, MARP de Ribeir&#227;o Preto, Sesc Amapá e Pinheiros&#47;SP, Pinacoteca de Sorocaba e de Botucatu&#47;SP, Casa de Cultura da Am&#233;rica Latina &#40;CAO&#41; e Museu Nacional da República em Bras&#237;lia, entre outros. Dois de seus trabalhos foram adquiridos pelos acervos da Pinacoteca de S&#227;o Paulo e Galeria Dez de Bras&#237;lia.
texto-descricao: CerradoF&#234;nix&#233; um registro oriundo das sobras de um inc&#234;ndio no ambiente do cerrado e da rápida rea&#231;&#227;o da natureza que surpreendentemente reage tal qual a ave f&#234;nix ressurgindo das próprias cinzas.Dan&#231;aSeca&#233; o triste registro de filhotes dos pássaros chamados Quero&#45;quero e que n&#227;o resistiram ao per&#237;odo da seca, morrendo aos poucos, provocando uma desesperada dan&#231;a de seus pais em torno dos filhotes que foram sucumbindo uma a um de uma ninhada de tr&#234;s. Esses pássaros n&#227;o desistem nunca de fazerem seus ninhos em locais expostos e mesmo com a perda de seus filhotes, demonstram muita resili&#234;ncia.Ninhos s&#227;o assustadores aglomerados de cupins que saem de suas tocas, buracos t&#227;o comuns nas terras duras do cerrado. Apesar de sua assustadora presen&#231;a, cumprem um papel importante no bioma do cerrado com uma fun&#231;&#227;o ecológica que inclui a aera&#231;&#227;o do solo, decomposi&#231;&#227;o de mat&#233;ria orgânica e reciclagem de nutrientes do solo. Seus ninhos oferecem abrigo para o habitat de diversas esp&#233;cies de animais e plantas.
ano: 2023
tecnica: Fotografia digital e v&#237;deo
---
<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="/assets/videos/CerradoF.mp4"></iframe>
</div>
  `Ninhos`<br><br>

  <img src="/assets/obras/CerradoF/1.png" alt="CerradoFênix" class="img-fluid d-block">
  `CerradoFênix`<br>  <br>
  <img src="/assets/obras/CerradoF/2.jpeg" alt="Dança" class="img-fluid d-block">
  `Dança`<br><br>


