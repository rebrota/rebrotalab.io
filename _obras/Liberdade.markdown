---
layout: obra
title: Liberdade! Liberdade!
thumbnail: /assets/thumbnail/t-Liberdade.png
artista: Robson Castro
l-bio: https&#58;&#47;&#47;linktr.ee&#47;robsoncastro.art.br
t-bio: Robson Castro &#40;1979&#41; vive e trabalha em Bras&#237;lia. Mestre em Artes pela UnB, &#233; artista do corpo e performer. À frente da Cia Inexistente, sua plataforma de cria&#231;&#227;o, abordou at&#233; o momento os temas origem, identidade e corpo nas cria&#231;&#245;es. Junto da ASQ e do Teatro do Concreto, participou de vários festivais de dan&#231;a e de teatro no Brasil. Na Anti Status Quo Companhia de Dan&#231;a &#40;ASQ&#41;, participou de pesquisas relacionadas a corpo e cidade e circulou pela Europa &#40;Su&#237;&#231;a, Eslov&#234;nia e S&#233;rvia&#41;.
texto-descricao: A videoperformance “Liberdade! Liberdade!” de Robson Castro faz parte da s&#233;rie Origens &#40;2020&#41;. Traz um homem caminhando em c&#237;rculos num descampado de terra vermelha. O t&#237;tulo traz uma ironia pois caminhar em c&#237;rculo talvez n&#227;o se traduza numa liberdade, mas em algum tipo de aprisionamento. Ou será que ele pode sair caminhando fora do c&#237;rculo quando quiser? Esse dilema &#233; a base para discutir liberdade, escolhas individuais e as amarras que nós mesmos nos propomos. E assim, seguimos caminhando na vida e em nossa História recente, seja com consci&#234;ncia delas, ou n&#227;o. A obra se relaciona com o tema desta web&#45;exposi&#231;&#227;o, Rebrota, pois &#233; preciso romper com esse ciclo que temos caminhado como humanidade, repensar sobre quando andamos em c&#237;rculo em rela&#231;&#227;o a diferentes temas e, em especial, ao meio ambiente. O quanto temos caminhado em c&#237;rculos? O quanto podemos &#40;e queremos&#41; fazer diferente? O quanto temos sido resistentes em mudar a rota para um mundo mais sustentável? Essas e outras perguntas podem ser talvez a chave para mudar de rota, antes que seja tarde demais.
ano: 2020
tecnica: videoperformance
---

<iframe width="832" height="468" src="https://www.youtube.com/embed/BlrM4teBQIY" title="Liberdade! Liberdade!  -  Robson Castro" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>