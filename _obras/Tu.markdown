---
layout: obra
title: Tu
thumbnail: /assets/thumbnail/t-Tu.png
artista: Ana Moravi
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;a.na.na.na&#47;
t-bio: Doutoranda em Artes Visuais pelo Instituto de Artes &#45; IDA&#47;UNB. Ana Moraes Vieira &#40;nome art&#237;stico Ana Moravi&#41; &#233; realizadora, produtora e pesquisadora, nascida em Belo Horizonte, MG. Vive e trabalha em Bras&#237;lia, Distrito Federal, com artes visuais, cinema e arte&#45;educa&#231;&#227;o. Atuou por 4 anos em Cabedelo, Para&#237;ba, onde desenvolveu projetos junto ao Instituto Federal da Para&#237;ba, a coletivos criativos e diversas cineastas. Mestre em Arte e Tecnologia da Imagem pela EBA&#47;UFMG. Produziu a Mostra Horizontes Transversais &#40;2014&#41;, lan&#231;ando o livro Horizontes Transversais &#45; artistas da imagem e som em MG &#40;2000&#45;2010&#41;, converg&#234;ncia da sua pesquisa de mestrado. Em Belo Horizonte, integrou o coletivo Col&#233;gio Invis&#237;vel como diretora e produtora, com trajetória de produ&#231;&#227;o com baixos or&#231;amentos, por meio de uma rede de parcerias e filmes exibidos e premiados em circuitos nacionais e internacionais de festivais, canais de TV no Brasil &#40;Canal Brasil&#41; e Am&#233;rica Latina &#40;Turner Broadcasting System&#41;. A MULHER QUE AMOU O VENTO &#40;2014, 67'&#41;, primeiro longa&#45;metragem como diretora, estreou na Mostra Aurora da 17 Mostra de Cinema de Tiradentes. Integrou o 30 Sal&#227;o Nacional de Arte de Belo Horizonte&#47;Bolsa Pampulha&#47;2010, o Museu do Sexo das Putas &#40;Funarte&#47;Aprosmig&#41;, a Bienal de Arte Digital 2018 entre outras exposi&#231;&#245;es coletivas. Foi vocalista na banda Madame Rrose Sèlavy.
texto-descricao: Pensado a partir da leitura dos ensaios de Flusser, em especial o texto Ventos, em que o autor aborda a natureza enquanto hierofania e mandamento transcendente, Tu &#40;2023&#41; &#233; um ensaio fotográfico que explora a fotografia de longa exposi&#231;&#227;o em diálogo com a cianotipia, para experimentar as dimens&#245;es de estranhamento e fugacidade na imagem e nas rela&#231;&#245;es entre nosso &#40;des&#41; entendimento das no&#231;&#245;es sobre a natureza e nossa exist&#234;ncia an&#237;mica. Tu exist&#234;ncia que se forja na imagem fotográfica e suas fic&#231;&#245;es transcendentes. Um transcender das limita&#231;&#245;es t&#233;cnicas, das rasuras conceituais, mas tamb&#233;m de qualquer f&#233; inabalável nas imagens. Tu aten&#231;&#227;o despretensiosa pelo percurso do processo criativo concretizada em uma caminhada pelo Parque Ecológico e de Uso Múltiplo Olhos D'Água &#40;Bras&#237;lia&#47;DF&#41; onde as imagens foram produzidas. Com sua funcionalidade representativa e protetiva de ecossistemas, os parques comp&#245;em essa natureza manejada, culturalizada, enredada nas leituras humanas de sua manifesta&#231;&#227;o imanente. Tu ant&#237;tese a priori e um afeto por consequ&#234;ncia, cujos mist&#233;rios residem na distância entre o espontâneo da vida e as trilhas demarcadas. Eis que nasce um corpo disforme, cuja natureza ultrapassa as linhas humanas. O cupim e a densa mata do cerrado contrap&#245;em sombras que parecem caminhar em dire&#231;&#227;o aos olhos, como se fossem capazes de cobrir a tela, para que ent&#227;o, descolados da vis&#227;o, possamos agu&#231;ar nossos ouvidos a escutar o vento. 
ano: 2023
tecnica: s&#233;rie fotográfica digital
---

<div class="menu d-none d-sm-block">


  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>
      <li data-target="#imagens" data-slide-to="7"></li>
      <li data-target="#imagens" data-slide-to="8"></li>
      <li data-target="#imagens" data-slide-to="9"></li>
      <li data-target="#imagens" data-slide-to="10"></li>
      <li data-target="#imagens" data-slide-to="11"></li>
      <li data-target="#imagens" data-slide-to="12"></li>
      <li data-target="#imagens" data-slide-to="13"></li>
      <li data-target="#imagens" data-slide-to="14"></li>
      <li data-target="#imagens" data-slide-to="15"></li>


     


   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/Tu/1.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Tu/2.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Tu/3.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Tu/4.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
            <div class="carousel-item">
        <img src="/assets/obras/Tu/5.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
            <div class="carousel-item">
        <img src="/assets/obras/Tu/6.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Tu/7.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Tu/8.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Tu/9.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Tu/10.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Tu/11.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Tu/12.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Tu/13.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Tu/14.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
		<div class="carousel-item">
        <img src="/assets/obras/Tu/15.jpg" alt="Tu" class="img-fluid mx-auto d-block">
      </div>
  </div>
 
 
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>





<div class="d-block d-sm-none">
  
  <img src="/assets/obras/Tu/1.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Tu/2.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Tu/3.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Tu/4.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Tu/5.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Tu/6.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Tu/7.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Tu/8.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Tu/9.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
     <img src="/assets/obras/Tu/10.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Tu/11.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Tu/12.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Tu/13.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Tu/14.jpg" alt="Tu" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Tu/15.jpg" alt="Tu" class="img-fluid" width="35%">
  
</div>