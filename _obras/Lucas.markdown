---
layout: obra
title: Lucas bororo 
thumbnail: /assets/thumbnail/t-Lucas.png
artista: Levi Tapuia
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;Levitapuia
t-bio: Me chamo Levi tapuia tenho 26 anos , sou ind&#237;gena do povo tapuias e vivo hoje dentro da favela do Itapo&#227; na capital federal , sou fotógrafo do movimento ind&#237;gena , comunicado do @midiaindigenaoficil , e fotógrafo o meu povo ind&#237;gena quando est&#227;o aqui no cerrado em Bras&#237;lia , para que atrav&#233;s das nossas lentes nossa história seja contado por nóis mesmo 
texto-descricao: Sou ind&#237;gena tapuia do sert&#227;o da Bahia , nascido no território ind&#237;gena Paranoá sul , dentro do cerrado , a foto mostra a minha realidade que nossa aldeia foi invadida e em cima das nossas terras constru&#237;do duas cidades , e com isso tendo um apagamento histórico de nóis povos ind&#237;genas do Df 
ano: 2019
tecnica: Fotografia 
---

<img src="/assets/obras/Lucas.jpeg" alt="Lucas bororo" class="img-fluid d-block">
