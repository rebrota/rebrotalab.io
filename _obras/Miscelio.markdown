---
layout: obra
title: Mic&#233;lio&#58; A internet da natureza | Plasmódio amarelo &#40;Blob&#41;
thumbnail: /assets/thumbnail/t-Miscelio.png
artista: Lucca Toschi
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;lu.ccaos
t-bio: Graduando em biologia pela Universidade Estadual de Goiás; desenvolve pesquisa com Ecologia de mixomicetos; Ilustrador &#40;aquarela e pintura a óleo&#41;; fotografo de microrganismos.
texto-descricao: Os fungos s&#227;o organismos incr&#237;veis, presentes em quase todos os lugares da biosfera, desempenhado papeis primordiais para o mantimento da vida na Terra. Por eles a mata se comunica consigo mesma e tudo se renova. &#124; Mixomicetos s&#227;o organismos sensacionais! Durante sua faze de plasmódio, seu comportamento e forma exótica intriga e encanta at&#233; mesmo quem está acostumado a trabalhar com eles. Provavelmente nunca tenha visto um por ai na natureza, más n&#227;o se engane, eles est&#227;o bem escondidinhos nos nossos vasos de planta, jardins e parques.O plasmódio da foto foi coletado em vaso de amoreira em uma resid&#234;ncia.
ano: 2023
tecnica: fotografia
---

   <img src="/assets/obras/Miscelio/1.png" alt="Plasmódio amarelo (Blob)" class="img-fluid mx-auto d-block">
     `Plasmódio amarelo (Blob)`<br>  <br>

   <img src="/assets/obras/Miscelio/2.png" alt="Micélio: A internet da natureza" class="img-fluid mx-auto d-block">
     `Micélio: A internet da natureza`<br>  <br>

      