---
layout: obra
title: Jacu | O inferno somos nós | Amarelo Esperança 
thumbnail: /assets/thumbnail/t-Jacu.png
artista: Bryan Carvalho
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;enquantohouverluz 
t-bio: De lá pra cá a fotografia me invadiu e passei a vive&#45;la, tanto profissionalmente, quanto nos momentos de lazer, em casa, na rua, na chuva, na fazenda ou numa casinha de sap&#234;.  Do meu ponto de vista, tudo pode ser fotog&#234;nico. Atualmente, al&#233;m do meu perfil comercial, mantenho alguns perfis de fotos de celular, com os quais já tive a oportunidade de ser exposto no MIS&#45;SP e publicado na extinta revista NoMirror Mag. Para al&#233;m do mero registro, a fotografia documenta, e , portanto, testemunha o mundo, nós, e o que fazemos com ele. Fotografar &#233; participar dos marcos temporais, &#233; intervir a partir da observa&#231;&#227;o. Por vezes &#233; denunciar ou preservar. A fotografia choca e tamb&#233;m encanta. Fotografias transformam. Por isso, fotografar tamb&#233;m &#233; um ato pol&#237;tico.
texto-descricao: O inferno somos nós&#34; &#233; um testemunho de uma &#233;poca em que &#233; comum na minha regi&#227;o inc&#234;ndios serem provocados em vários pontos da cidade, por vezes invadindo a regi&#227;o urbanizada e portanto retrata o efeito nocivo do humano sobre a vegeta&#231;&#227;o. &#34;Jacu&#34; &#233; uma ave que pode ser encontrada tanto no Cerrado como na Mata Atlântica, esse especialmente posava sobre uma árvore de cerrado numa regi&#227;o especialmente contemplada por ambas as formas de vegeta&#231;&#227;o e amea&#231;adas pelas mineradoras na Serra do Curral. &#34;Amarelo Esperan&#231;a&#34; &#233; um trocadilho com &#34;Verde Esperan&#231;a&#34;, em homenagem ao icônico Ip&#234; Amarelo, que se destaca em meio a vegeta&#231;&#227;o. O tratamento pálido dado a fotografia junto ao destaque natural do amarelo deve remeter a &#34;aten&#231;&#227;o&#34;. Essa exposi&#231;&#227;o parece que caiu do c&#233;u, porque há tempos que essas obras est&#227;o prontas e nunca tive a oportunidade de mostra&#45;las em contexto apropriado com a relevância de causa, para al&#233;m da est&#233;tica. 
ano: 2016 &#45; 2018 &#45; 2020
tecnica: Fotografia
---

   <img src="/assets/obras/Jacu/1.jpg" alt="Jacu" class="img-fluid mx-auto d-block">
     `Jacu`<br>  <br>

   <img src="/assets/obras/Jacu/3.jpg" alt="O inferno somos nós" class="img-fluid mx-auto d-block">
     `Micélio: A internet da natureza`<br>  <br>

   <img src="/assets/obras/Jacu/2.jpg" alt="Amarelo Esperança" class="img-fluid mx-auto d-block">
     `Micélio: A internet da natureza`<br>  <br>

      