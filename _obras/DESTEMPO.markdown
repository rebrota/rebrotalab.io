---
layout: obra
title: DESTEMPO &#45; ENTRE ÁGUAS, MEMÓRIAS E PERTENCIMENTO
thumbnail: /assets/thumbnail/t-DESTEMPO.png
artista: Mari Gemma De La Cruz
l-bio: www.marigemma.com
t-bio: Artista visual desde 2016. Iniciou aos 53 anos e desde ent&#227;o realiza trabalhos com autorretrato, fotoperformance, v&#237;deo, instala&#231;&#227;o e interven&#231;&#227;o em espa&#231;os naturais e urbanos, centrando sua pesquisa nos processos ps&#237;quicos que permeiam seu cotidiano e que fragilizam sua sensibilidade corpórea.  Sua temática está ligada às quest&#245;es autobiográficas relacionadas à mulher, ao feminismo e às quest&#245;es ambientais, numa perspectiva biopsicosocioambiental. Antes foi farmac&#234;utica industrial, mestre em Saúde e Ambiente. Recebeu premia&#231;&#245;es, realizou exposi&#231;&#245;es nacionais e internacionais. Vive em Cuiabá, Brasil.
texto-descricao: Atrav&#233;s da performance nas águas do cerrado entre Cuiabá e Chapada dos Guimar&#227;es &#40;Mato Grosso&#41; e de retratos de álbuns familiares, realizo um ritual de cura utilizando flores de Macela &#40;Achyrocline satureoindes&#41;, planta medicinal que faz parte da memória afetiva de cuidados na minha infância no Rio Grande do Sul e tamb&#233;m encontrada nas morrarias às margens do rio Cuiabá. A memória epigen&#233;tica, herdada pela linhagem materna pode determinar o aparecimento de enfermidades, ao ser ativada por hábitos de vida, exposi&#231;&#245;es ambientais inadequadas e traumas, entre eles destaco a guerra, a viol&#234;ncia familiar, o abuso &#40;sexual, f&#237;sico e psicológico&#41; e as migra&#231;&#245;es. Minha m&#227;e passou vivenciou a guerra e a migra&#231;&#227;o para o Brasil e eu vivi o processo de deslocamento do sul do Brasil para o Centro&#45;Oeste, al&#233;m da recorda&#231;&#227;o de abuso sofrido na infância, após 40 anos, o que determinou o apagamento de recorda&#231;&#245;es da infância e adolesc&#234;ncia. Estas viv&#234;ncias determinaram na árvore familiar altera&#231;&#245;es que interferiram no processo saúde&#45;doen&#231;a dos descendentes e podem se manifestar na descend&#234;ncia. Minha história segue o fio do destempo entre gera&#231;&#245;es de mulheres. Dizem que a água tem memória e que ela guarda toda a história da humanidade e portanto todo o conhecimento. Ao reconstruir minha memória, seguindo a linhagem familiar, refa&#231;o minha identidade e nas águas busco o pertencimento e cura, ressignifico a memória epigen&#233;tica para sanar as gera&#231;&#245;es vindouras..
ano: 2023
tecnica: v&#237;deoarte
---

<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="/assets/videos/Destempo.mp4"></iframe>
</div>
  `DESTEMPO : ENTRE ÁGUAS, MEMÓRIAS E PERTENCIMENTO`<br><br>
