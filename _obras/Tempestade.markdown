---
layout: obra
title: Tempestade
thumbnail: /assets/thumbnail/t-Tempestade.png
artista: Bárbara de Azevedo
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;barbaradeazevedo_videoarte&#47;
t-bio: Bacharela em artes visuais pela Universidade Belas Artes de S&#227;o Paulo, desde 2007 atua com videoarte e anima&#231;&#245;es experimentais, possui obras premiadas e selecionadas em exposi&#231;&#245;es, mostras e festivais nacionais e internacionais.
texto-descricao: A imin&#234;ncia de uma tempestade que no conceito do v&#237;deo dialoga com a problemática dos desequil&#237;brios ambientais que as mudan&#231;as climáticas est&#227;o trazendo ao cerrado, como a escassez de chuvas. 
ano: 2008
tecnica: Videoarte
---

<iframe width="832" height="468" src="https://youtu.be" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>