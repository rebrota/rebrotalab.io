---
layout: obra
title: L&#237;quido Vital
thumbnail: /assets/thumbnail/t-Liquido.png
artista: Patr&#237;cia Siqueira
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;patriciasiqueira.art
t-bio: Artista, bailarina, coreógrafa, atriz e performer. Elabora trabalhos h&#237;bridos, utilizando&#45;se do cruzamento de linguagens desenvolvidas ao longo de sua carreira art&#237;stica, tais como dan&#231;a, teatro, performance, v&#237;deo arte, v&#237;deo performance. Atualmente, dentro das artes plásticas tem conduzido seus trabalhos e pesquisas para o desenho expandido, trazendo sua memória de fluxo, de movimento, de ocupa&#231;&#227;o e composi&#231;&#227;o do espa&#231;o, de improvisa&#231;&#227;o e de composi&#231;&#227;o da cena no momento.
texto-descricao: A natureza mostra a sua for&#231;a orgânica e determinada. O Cerrado &#233; considerado o ber&#231;o das águas, pois grande parte dos principais rios nascem em seu  território. Carrega em seu seio uma potente biodiversidade.  O videoarte “Liquido Vital” revela metaforicamente como a m&#227;o do homem vem interferindo na estrutura dos rios, desarticulando, desestabilizando e desorganizando todo o seu conteúdo, afetando a popula&#231;&#227;o,  a flora e fauna do lugar. A água  como elemento vital deste planeta.  
ano: 2023
tecnica: videoarte
---
<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="/assets/videos/Liquido.mp4"></iframe>
</div>
  `Líquido Vital`<br><br>
