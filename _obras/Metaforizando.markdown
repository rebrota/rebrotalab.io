---
layout: obra
title: Metaforizando o óbito de um ecossistema
thumbnail: /assets/thumbnail/t-Metaforizando.png
artista: Guilherme Campos 
l-bio: instagram.com&#47;guilhe_art
t-bio: Guilherme Campos nasceu em S&#227;o Paulo mas vive em Goiânia. Na infância, já demonstrava grande afinidade com o papel e o lápis e foi a jun&#231;&#227;o de sua paix&#227;o pela leitura e pela natureza que despertou seu interesse pela arte. O fasc&#237;nio pela pr&#233;&#45;história &#233; tema recorrente em suas obras, angariando&#45;lhe premia&#231;&#245;es na área, mas foram as ru&#237;nas gregas que determinaram seu caminho para a escolha da arquitetura como forma&#231;&#227;o. Tal curso permitiu ampliar seu universo imag&#233;tico, tra&#231;os vis&#237;veis em suas ilustra&#231;&#245;es denotadas pelo uso de perspectivas complexas e grande riqueza de detalhes, em especial a anatomia. O artista vem realizando exposi&#231;&#245;es coletivas desde 1999, ilustrando artigos cient&#237;ficos, livros e acumula duas premia&#231;&#245;es nacionais.
texto-descricao: Um s&#237;mbolo do cerrado, jogado ao acostamento, depois de sofrer uma covarde colis&#227;o com um aparato mecânico movido a combust&#237;veis fósseis, passa sem trepidar e acomete o tamanduá de sua sina antes da hora. Assim como o cerrado, ele representa o descaso humano com este bioma t&#227;o belo, t&#227;o rico, t&#227;o resultante das sobreviv&#234;ncias às intemp&#233;ries desafiadoras. O óbito do ecossistema se faz de forma presente, e assim mesmo, poucos est&#227;o com suas m&#227;os postas ao trabalho, pra desfazer ou frear a marcha do dito progresso, que passa em cima de seres vivos, moldados por milh&#245;es de anos pra que tecnologias de poucas d&#233;cadas possam tomar seus lugares e uma hora serem substitu&#237;das por mais lixo humano. 
ano: 2023
tecnica: Xilogravura
---

<img src="/assets/obras/Metaforizando.jpeg" alt="Metaforizando o óbito de um ecossistema" class="img-fluid d-block">
<br>`700x1070 mm`
