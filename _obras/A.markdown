---
layout: obra
title: À espera de nuvens
thumbnail: /assets/thumbnail/t-A.png
artista: Fabr&#237;cio Dias
l-bio: www.fabriciodias.46graus.com
t-bio: Fabr&#237;cio Dias Livramento de Nossa Senhora, Bahia, Brasil, 1996 &#47; Vive e trabalha em Livramento &#45; BA. Fabr&#237;cio Dias Medeiros &#233; Artista visual e pesquisador.  É Bacharel em Artes Plásticas pela Escola de Belas Artes – EBA&#47;UFBA &#40;2021&#41;, atualmente &#233; mestrando em Artes Visuais na linha de pesquisa em Processos Criativos no PPGAV&#45;EBA&#47;UFBA, onde desenvolve pesquisa sobre as memórias da Estrada Real, trecho que liga os munic&#237;pios baianos de Livramento e Rio de Contas. Inquieto e curioso, coleta e cataloga objetos enquanto caminha a procura de memórias como um &#34;arqueólogo visual&#34;. Estes objetos coletados s&#227;o utilizados pelo artista em experimenta&#231;&#245;es visuais no desenvolvimento de obras em multilinguagens e t&#233;cnicas. Sua pesquisa envolve a presen&#231;a da memória, ancestralidade e cultura do sert&#227;o nordestino.  
texto-descricao: A instala&#231;&#227;o &#233; constitu&#237;da a partir de materiais e objetos coletados pelo artista entre os anos de 2019 e 2020 durante caminhadas por comunidades sertanejas, localizadas no interior da cidade de Livramento de Nossa Senhora &#45; BA. A instala&#231;&#227;o retrata de forma metafórica a espera dos agricultores  pela vinda da chuva, representada pelo gotejar da água que cai da parte superior da instala&#231;&#227;o que irriga o solo e faz brotar as sementes, num processo po&#233;tico e visual de transforma&#231;&#227;o e espera.
ano: 2021
tecnica: Instala&#231;&#227;o
---


<div class="menu d-none d-sm-block">


  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>



   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/A/1.jpg" alt="À espera de nuvens" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/A/2.jpg" alt="À espera de nuvens" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/A/3.jpg" alt="À espera de nuvens" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/A/4.jpg" alt="À espera de nuvens" class="img-fluid mx-auto d-block">
      </div>
  </div>
 
 
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>





<div class="d-block d-sm-none">
  
  <img src="/assets/obras/A/1.jpg" alt="À espera de nuvens" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/A/2.jpg" alt="À espera de nuvens" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/A/3.jpg" alt="À espera de nuvens" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/A/4.jpg" alt="À espera de nuvens" class="img-fluid" width="35%">
  
</div>