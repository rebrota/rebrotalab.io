---
layout: obra
title: Mat&#227;o 
thumbnail: /assets/thumbnail/t-Matao.png
artista: Veronica Lindquist
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;vero.quist
t-bio: Verônica Lindquist, nasceu em Luc&#233;lia&#45;SP, em novembro de 1987. Vive e trabalha em Campo Grande&#45; MS. Artista visual, escritora, arte educadora e pesquisadora.
texto-descricao: A proposta surgiu de uma busca por construir uma po&#233;tica visual junto a uma casa abandonada no Cerrado. Quando reencontrei&#45;me com as fotografias em uma pasta de arquivo perdida no HD, resignifiquei a memória e tentei imaginar o que buscava tentar entender naquele momento do passado. Eu tamb&#233;m abandonei esse projeto lá trás. E reutilizar essas fotografias, assim como escrever uma poesia desse reencontro depois de onze anos, me fez rever sobre o tempo e o desdobramento das  experi&#234;ncias diante de meu entendimento no presente. Ent&#227;o, todo o processo foi de resignificar as rela&#231;&#245;es com uma casa abandonada no Cerrado que foi encontrada no ano de 2011 e que, talvez hoje ela n&#227;o exista mais, e seja apenas revivida in memoriam em 2023 por meio dessa produ&#231;&#227;o de v&#237;deoarte, videopoesia. A abordagem est&#233;tica do v&#237;deo intenta criar um curto&#45;circuito entre&#58; imagens, palavras, memória, desenhos, paisagem, tempo e a poesia no meio de tudo isso juntando as metáforas em consonância das experimenta&#231;&#245;es tecnológicas junto ao meio ambiente. 
ano: 2023
tecnica: videoarte
---
<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="/assets/videos/MATAO.mp4"></iframe>
</div>
  `Matão`<br><br>