---
layout: obra
title: Um Corpo ao Chão
thumbnail: /assets/thumbnail/t-Um.png
artista: Gustavo Bays
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;fotografiaporgustavobays
t-bio: Trabalho com Fotografia desde 2011, a partir de 2011 participei de algumas exposi&#231;&#245;es fotográficas coletivas, exposi&#231;&#245;es com sele&#231;&#245;es pr&#233;vias e a convite. Em 2019 participei de uma exposi&#231;&#227;o no Texas e New Orleans com uma fotografia na t&#233;cnica de Cianotipia. Em 2020 produzi a primeira exposi&#231;&#227;o individual com uma s&#233;rie fotográfica intitulada &#34;Há Penas Poss&#237;veis&#34;. Em 2021 fiquei em segundo lugar em um Concurso de Fotografia em Bras&#237;lia, na categoria Preto e Branco. em 2022 venci um concurso nacional de fotografia realizado pela UEG campus Anápolis com temática ambiental, sobre o cerrado. Em 2022 participei de um concurso nacional de fotografia do site Iphoto Chanel e fiquei em primeiro com mais de 17 mil votos, de um total de 20 mil. Em 2023 participei de uma exposi&#231;&#227;o coletiva, novamente de cianotipia, no Tenessee. Ainda em 2023 participei do Sal&#227;o de Arte Conteporânea de Formosa, exposi&#231;&#227;o com artistas selecionados do Brasil todo, realizado pela IFG campus Formosa.
texto-descricao: A s&#233;rie “Um Corpo ao ch&#227;o” nos apresenta de forma metafórica uma das principais causas da destrui&#231;&#227;o &#40;ou morte&#41; do cerrado, a monocultura. Entre as linhas padronizadas, retas, do que foi escolhido para substituir o cerrado naquele espa&#231;o  se apresentam as linhas espontâneas, de curvas livres e infinitas, mostrando a beleza que ali habitava. A for&#231;a do cerrado aparece na resist&#234;ncia destas que foram as últimas, que mesmo mortas permaneceram inteiras. Resistiram, mas o constru&#237;do substituiu o natural. As de linhas retas que se imp&#245;em permanecem de p&#233; olhando aquela cheia de curvas que agonizou.
ano: 2022
tecnica: Fotografia
---
<div class="menu d-none d-sm-block">


  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>
     


   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/Um/1.jpg" alt="Um Corpo ao Chão" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Um/2.jpg" alt="Um Corpo ao Chão" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Um/3.jpg" alt="Um Corpo ao Chão" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Um/4.jpg" alt="Um Corpo ao Chão" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Um/5.jpg" alt="Um Corpo ao Chão" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Um/6.jpg" alt="Um Corpo ao Chão" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Um/7.jpg" alt="Um Corpo ao Chão" class="img-fluid mx-auto d-block">
      </div>
      
  </div>
 
 
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>





<div class="d-block d-sm-none">
  
  <img src="/assets/obras/Um/1.jpg" alt="Um Corpo ao Chão" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Um/2.jpg" alt="Um Corpo ao Chão" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Um/3.jpg" alt="Um Corpo ao Chão" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Um/4.jpg" alt="Um Corpo ao Chão" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Um/5.jpg" alt="Um Corpo ao Chão" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Um/6.jpg" alt="Um Corpo ao Chão" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Um/7.jpg" alt="Um Corpo ao Chão" class="img-fluid" width="35%"><br><br>
  
</div>
