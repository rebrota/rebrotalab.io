---
layout: obra
title: Brasília&#58; a capital do Cerrado
thumbnail: /assets/thumbnail/t-Brasilia.png
artista: Vin&#237;ciu Fagundes &#45; Estúdio Tutóia
l-bio: https&#58;&#47;&#47;estudiotutoia.com.br&#47;      
t-bio: Engenheiro ambiental pela Pontif&#237;cia Universidade Católica de Goiás &#40;PUC&#45;Goiás&#41;, Mestre em Engenharia do Meio Ambiente e Doutor em Ci&#234;ncias Ambientais pela Universidade Federal de Goiás &#40;UFG&#41;, há treze anos Vin&#237;ciu Fagundes Bárbara &#233; professor efetivo do Instituto Federal de Goiás &#40;dos cursos de Engenharia Ambiental e Sanitária e T&#233;cnico em Controle Ambiental&#41;, e tamb&#233;m Perito Ambiental do Minist&#233;rio Público de Goiás, institui&#231;&#245;es onde atua cient&#237;fica e tecnicamente em diversos temas ligados à área ambiental.A partir de 2020, com o advento da pandemia de COVID&#45;19, se permitiu come&#231;ar a se expressar artisticamente de forma autodidata. Desdeent&#227;o, idealizou e mant&#233;m o Estúdio Tutóia &#40;@estudio_tutoia&#41;, por interm&#233;dio do qual cria obras de arte popular com elementos naturais &#40;descartes vegetais&#41; dos biomas brasileiros, notadamente do Cerrado, que utiliza para fins de sensibiliza&#231;&#245;es e reflex&#245;es art&#237;sticas, ambientais e culturais.Por interm&#233;dio da t&#233;cnica assemblage, o artista concebe obras exclusivas que exaltam cores, formas e texturas de elementos botânicos variados que, associados a interven&#231;&#245;es com tintas e outros materiais &#40;como metais e madeira de demoli&#231;&#227;o, dentre outros&#41;, n&#227;o somente explicitam a beleza e a importância&#47;relevância ambiental dos ecossistemas brasileiros, mas tamb&#233;m nos relembram que somos parte da natureza, ou seja, somos, todos, seres que integram um coletivo planetário profundamente interdependente. Seus trabalhos t&#234;m ganhado destaque em diversos Estados do Brasil, em pa&#237;ses como Inglaterra e Portugal e, tamb&#233;m, em eventos como CASACOR Goiás, dentre dezenas de outros. Recentemente, teve seis obras adquiridas pela TV Globo, as quais atualmente se encontram no cenário da novela Terra e Paix&#227;o.
texto-descricao: Bras&#237;lia&#58; a capital do Cerrado' &#233; uma obra inspirada no Plano&#45;Piloto da Capital Federal do Brasil, e tamb&#233;m no bioma que a abriga&#58; o Cerrado. É uma tradu&#231;&#227;o art&#237;stica da profunda interrela&#231;&#227;o entre natureza e homem, entre sociedade e meio ambiente. Por interm&#233;dio da vontade humana, Bras&#237;lia &#34;brotou&#34; no Cerrado. Por sua vez, o Cerrado rebrota todos os anos em Bras&#237;lia, em um eterno vai&#45;e&#45;vem de cores, formas e texturas vegetais que &#40;ainda&#41; nos brindam generosamente &#40;ainda&#41;, apesar de tudo.Bras&#237;lia&#58; a capital do Cerrado foi criada com elementos de Mogno; fibras de Buriti e de Tucum e Sempre&#45;Vivas, oriundos de esp&#233;cies fortes, resistentes, icônicas, belas, representativas, simples, delicadas e marcantes... Elementos coletados na regi&#227;o do Distrito Federal e cuidadosamente escolhidos para simbolizar a for&#231;a a dicotomia homem x meio ambiente. Ou seria homem = meio ambiente?
ano: 2023
tecnica: Quadro&#45;escultura &#40;assemblage&#41;
---
<div class="menu d-none d-sm-block">


  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
     


   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/Brasilia/1.jpg" alt="Brasília: a capital do Cerrado" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Brasilia/2.png" alt="Brasília: a capital do Cerrado" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Brasilia/3.jpg" alt="Brasília: a capital do Cerrado" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Brasilia/4.jpg" alt="Brasília: a capital do Cerrado" class="img-fluid mx-auto d-block">
      </div>
  </div>
 
 
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>





<div class="d-block d-sm-none">
  
  <img src="/assets/obras/Brasilia/1.jpg" alt="Brasília: a capital do Cerrado" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Brasilia/2.png" alt="Brasília: a capital do Cerrado" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Brasilia/3.jpg" alt="Brasília: a capital do Cerrado" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Brasilia/4.jpg" alt="Brasília: a capital do Cerrado" class="img-fluid" width="35%">
  
</div>