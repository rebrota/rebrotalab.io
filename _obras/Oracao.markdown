---
layout: obra
title: Oração a Santo Expedito
thumbnail: /assets/thumbnail/t-Oracao.png
artista: Felipe Braga
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;felipebraga____
t-bio: Artista visual de Duque de Caxias, Baixada Fluminense, inicio minha carreira atrav&#233;s da fotografia, campo da minha forma&#231;&#227;o acad&#234;mica. Na minha pesquisa, que se desdobra tamb&#233;m na colagem, instala&#231;&#227;o e escultura, proponho temas relacionados à minha viv&#234;ncia nômade pela Am&#233;rica Latina.
texto-descricao: Parto do registro de uma on&#231;a&#45;preta para criar esta colagem, agregando imagens de Santo Expedito, numa tentativa de pedido de socorro dos biomas brasileiros frente à destrui&#231;&#227;o sofrida ao longo de s&#233;culos de invas&#245;es.
ano: 2023
tecnica: Colagem
---

<img src="/assets/obras/Oracao.jpeg" alt="Oração a Santo Expedito" class="img-fluid d-block">
<br>
`300x195 mm` 