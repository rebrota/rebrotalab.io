---
layout: obra
title: Terra, Brasilidade
thumbnail: /assets/thumbnail/t-Terra.png
artista: Philipe Kaoká 
l-bio: kaoka.com.br
t-bio: Philipe Kaoká &#233; artista visual, Macumbeiro e cruzador de histórias. Traz em seu processo criativo, express&#245;es, contos e movimentos relacionados a cultura popular, ind&#237;gena e de terreiro. Inspirado nos cruzamentos culturais, busca dar luz a pot&#234;ncia de uma Brasilidade em resignificar s&#237;mbolos, histórias e divindades, de um Brasil institucional, em seu favor. As pinturas s&#227;o encruzilhada, s&#227;o mironga, s&#227;o resgate. Um encantamento contra um brasil colonial. Um convite a ver e ouvir uma vers&#227;o popular das tradi&#231;&#245;es brasileiras.
texto-descricao: A árvore &#233; ancestral, as etnias ind&#237;genas s&#227;o ancestrais. Suas ra&#237;zes profundas tem longo relacionamento com essa terra. Terra que nos alimenta, terra que nos faz abundantes e fortes. Sem teu seio, ficamos fracos, secos e rasos.cheios de escassez. Nela sentimos o passar das esta&#231;&#245;es, o velho se vai, o novo chega, gera&#231;&#227;o por gera&#231;&#227;o. Mantendo sua raiz, seu v&#237;nculo, assim, o velho n&#227;o morre. O novo já nasce velho. Em tempos de seca, &#233; na velha árvore que encontramos água. Sumaúma, Baobá &#45; a mironga da longevidade e resist&#234;ncia daqueles que precisam matar a sede na seca. Viva Sumaúma, viva o Baobá!!! <br><br> Salve os povos daqui e de lá !!!<br> Salve os encantados e encantadas !!!<br> Salve todo juremá!!! &#124; No avesso de um Brasil institucional existem outros Brasis, Brasileiros cheios de brasilidade.<br> Lugar de um c&#233;u estrelado de búzios que quando caem, norteiam destinos. Ch&#227;o de barro, de ro&#231;a, terreiro, pau a pique. De patuás, mandingas e rezadeiras. De mata encatanda. De ci&#234;ncia da floresta. De banho de folhas. Pajelan&#231;a, De defumador, de garrafadas, chás, xaropes e Maracás. <br> Um Brasil feito de ax&#233;!!! <br> Viva a Brasilidade !!! <br> Viva esse Brasil Brasileiro!!!
ano: 2021
tecnica: Pintura
---

<img src="/assets/obras/Terra/1.png" alt="Terra" class="img-fluid d-block">
`Terra`<br><br>

<img src="/assets/obras/Terra/2.png" alt="Brasilidade" class="img-fluid d-block">
`Brasilidade`

