---
layout: obra
title: O todo que liga tudo
thumbnail: /assets/thumbnail/t-Otodo.png
artista: Pirá Arte
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;_pirarte
t-bio: Nascida no ano 2000 em S&#227;o Paulo&#45;SP por&#233;m desde antes do primeiro anos de vida criada em Itu&#45;SP e atualmente residindo em Bauru&#45;SP, formada como Bacharel em Artes Visuais pela UNESP&#45;Bauru em 2022, atualmente cursando Licenciatura em Artes Visuais tamb&#233;m pela UNESP&#45;Bauru. Minha po&#233;tica pessoal e pesquisa art&#237;stica seguem em meio às tentativas, procurando trazer para o campo visual a valoriza&#231;&#227;o multiesp&#233;cie, pensando sobre o olhar atento e sens&#237;vel para aquilo que está em nossa volta e que muitas vezes passa despercebido. Vejo na arte oportunidade de investiga&#231;&#245;es relacionais, multiesp&#233;cie, trazendo&#45;as para o espa&#231;o est&#233;tico, po&#233;tico, sens&#237;vel e externo, por&#233;m, tamb&#233;m as tendo como um meio pol&#237;tico do agir.  Atrav&#233;s da Arte dou voz ao meu modo de ver o mundo, tendo as manchas de tinta nesta aresta entre arte e realidade.
texto-descricao: A obra dialoga com o jardim de minha avó e com lembran&#231;as que permeiam o existir. Busquei transmitir a ideia de uma rela&#231;&#227;o poss&#237;vel entre aquilo que difere &#40;como esp&#233;cie, pensando a composi&#231;&#227;o entre plantas e animal&#41;, como uma esp&#233;cie de mutualismo existencial, ao passo que, a imagem central do papagaio&#45;verdadeiro &#40;presente de modo abrangente no Brasil, inclusive em áreas de Cerrado&#41; comunica um convite, como se quem estivesse como espectador se sentisse parte desse todo que comp&#245;e a tela, convidado ao abra&#231;o da asa aberta, isto &#233;, se a dinâmica relacional se transforma torna&#45;se poss&#237;vel o abra&#231;o imaterial entre a diferen&#231;a. Creio que a obra dialoga com a temática da exposi&#231;&#227;o, ao passo que, &#233; necessário olhar para as mais diversas formas de vida presente para que as perspectivas sejam inundadas e afetadas conduzindo os indiv&#237;duos à consci&#234;ncia e, por consequ&#234;ncia, tamb&#233;m a atitudes de preserva&#231;&#227;o, acredito que falar do Cerrado, ou dos Cerrados &#233; tornar grande toda a riqueza, diversidade, beleza, mas tamb&#233;m evocar sempre a necessidade de uma outra rela&#231;&#227;o entre humanos e natureza, para que essa beleza e grandeza n&#227;o se torne apenas uma memória estampada em telas de pintura. 
ano: 2023
tecnica: Pintura
---

<div class="menu d-none d-sm-block">


  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
     


   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/Otodo/1.png" alt="O todo que liga tudo" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Otodo/2.jpg" alt="O todo que liga tudo" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Otodo/3.jpg" alt="O todo que liga tudo" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Otodo/4.jpg" alt="O todo que liga tudo" class="img-fluid mx-auto d-block">
      </div>
  </div>
 
 
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>





<div class="d-block d-sm-none">
  
  <img src="/assets/obras/Otodo/1.png" alt="O todo que liga tudo" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Otodo/2.jpg" alt="O todo que liga tudo" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Otodo/3.jpg" alt="O todo que liga tudo" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Otodo/4.jpg" alt="O todo que liga tudo" class="img-fluid" width="35%">
  
</div>