---
layout: obra
title: Desolação 
thumbnail: /assets/thumbnail/t-Desolacao.png
artista: Merielin Gomes 
l-bio: https&#58;&#47;&#47;instagram.com&#47;merielingomes?igshid=MzRlODBiNWFlZA==
t-bio: Sou uma artista ambiental, moro na cidade de Sorocaba&#47;SP.  Fiz faculdade de Arte, especializa&#231;&#227;o em Arteterapia e Psicopedagogia, e sempre fui apaixonada pela natureza.Minhas cria&#231;&#245;es evocam nossa ancestralidade. A importância da natureza e utilizo em minhas composi&#231;&#245;es diversos elementos naturais, como galhos, folhas e outros materiais que a natureza &#34;descarta&#34;, al&#233;m de resina e pigmentos, que trazem um efeito orgânico em minhas obras.
texto-descricao: Obra&#58; &#34;Desola&#231;&#227;o&#34; Obra inspirada nas queimadas que destroem nossas florestas e desequilibram o meio ambiente. O desmatamento do Cerrado &#233; um fenômeno com aumento expressivo nos últimos anos. N&#227;o podemos aceitar que um dos biomas mais ricos e diversos do mundo sofra com as a&#231;&#245;es humanas. Cabe a cada um de nós, buscar recursos para minimizar os efeitos nocivos ao nosso Cerrado.
ano: 2023
tecnica: Pintura e fotografia com t&#233;cnica mista.
---

<img src="/assets/obras/Desolacao.jpeg" alt="Desolação" class="img-fluid d-block">
<br>`500x500 mm`
