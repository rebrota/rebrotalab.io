---
layout: obra
title: Não jogue este em vias públicas&#34;, &#34;T&#227;o longe, t&#227;o perto&#34;, O amanh&#227; que n&#227;o deseja chegar
thumbnail: /assets/thumbnail/t-Jogue.png
artista: Michelle Duarte
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;mirada.art
t-bio: Michelle Duarte &#233; artista visual em Goiânia, GO, onde reside e trabalha. Em sua obra utiliza a liberdade de t&#233;cnicas e suportes diversos para discutir temáticas sens&#237;veis, questionando poeticamente os limites entre dualidades. Morte e vida, presente e futuro, usual e absurdo, e outras fronteiras, s&#227;o marcados, em seu fazer art&#237;stico, por uma subjetividade que, frequentemente, apoia&#45;se no universo surreal e imaginário para ganhar representa&#231;&#227;o. Formas orgânicas e abstratas convivem num exerc&#237;cio cont&#237;nuo de aproxima&#231;&#227;o entre aparentes opostos na perspectiva de valores e hábitos comuns a qualquer viv&#234;ncia humana.
texto-descricao: A obra problematiza as a&#231;&#245;es humanas sobre o cerrado que, n&#227;o apenas descartam seus rejeitos no bioma, mas o tornam o próprio rejeito, para o qual se nega o olhar de cuidado e preserva&#231;&#227;o. Para a colagem, utiliza&#45;se uma xilogravura com a imagem de uma árvore do cerrado. A imagem &#233; rasgada ao meio, separada em partes opostas e uma delas &#233; parcialmente recortada em tiras para, atrav&#233;s da t&#233;cnica de rollage, entrela&#231;arem&#45;se ao papel, riscado e cortado em perspectiva. A imagem obtida nesse processo remete aos pr&#233;dios de uma cidade, numa alus&#227;o à depend&#234;ncia estrutural que temos do bioma. Ao mesmo tempo, em palavras pequenas recortadas de um panfleto que &#233; normalmente distribu&#237;do nas ruas, l&#234;&#45;se a frase “n&#227;o jogue este em vias públicas”. Questiona&#45;se, assim, o tratamento que nossa sociedade tem dado ao cerrado, como um bem descartável, capaz de sempre “reciclar&#45;se”. Mas at&#233; quando? &#124; A obra deseja provocar reflex&#227;o acerca do nosso olhar para o cerrado e sua destrui&#231;&#227;o gradual. A xilogravura de uma árvore t&#237;pica do cerrado foi impressa em guardanapo de papel, denotando a fragilidade desse bioma nas m&#227;os do nosso descaso. Um descaso que &#233; proporcional ao  distanciamento que insistimos manter em rela&#231;&#227;o a temas que demandam drásticas mudan&#231;as em nosso comportamento social. Por&#233;m, n&#227;o est&#227;o nada distantes de nós as consequ&#234;ncias do desmatamento, da polui&#231;&#227;o, do genoc&#237;dio de povos originários, representados pelas cores quentes ao fundo e as linhas do bordado. As palavras coladas constroem a po&#233;tica de um c&#237;rculo vicioso, que denuncia o paradoxo de um distanciamento fict&#237;cio. Afinal, a destrui&#231;&#227;o do cerrado está altamente intrincada e relacionada ao nosso cotidiano. &#124; A obra discute o impacto da a&#231;&#227;o humana sobre o cerrado numa perspectiva de disputa entre dois polos&#58; de um lado o futuro nefasto imposto pela devasta&#231;&#227;o, do outro, a resist&#234;ncia do bioma. Por meio de t&#233;cnicas mistas, “O amanh&#227; que n&#227;o deseja chegar” busca representar a luta do cerrado contra um tempo impiedosamente imposto pela degrada&#231;&#227;o humana, onde a biodiversidade tenta se refazer, mas segue altamente amea&#231;ada. O amanh&#227; que se desenha nesse horizonte já se sabe perverso, e n&#227;o deseja chegar, como uma profecia de destrui&#231;&#227;o que n&#227;o deseja cumprir&#45;se, como a própria morte que se recusa a esse papel diante da vida exuberante. Afinal, aqui n&#227;o &#233; a morte que mata, mas &#233; a m&#227;o humana, suja de terra seca, carv&#227;o e sangue. 
ano: 2023
tecnica: Colagem
---

  
  <img src="/assets/obras/Jogue/1.jpeg" alt="Não jogue este em vias públicas" class="img-fluid d-block">
  `Não jogue este em vias públicas`<br>  <br>
  <img src="/assets/obras/Jogue/2.jpeg" alt="Tão perto, tão longe" class="img-fluid d-block">
  `Tão perto, tão longe`<br><br>
  <img src="/assets/obras/Jogue/3.jpeg" alt="O amanhã que não deseja chegar" class="img-fluid d-block">
  `O amanhã que não deseja chegar`<br><br>

  
