---
layout: obra
title: VOLTAR AOS PASSOS QUE FORAM DADOS
thumbnail: /assets/thumbnail/t-VOLTAR.png
artista: ryan hermogenio
l-bio: https&#58;&#47;&#47;ryanhermogenio.weebly.com&#47;
t-bio: Ryan Hermogenio &#233; artista, reside no Rio Comprido&#45;RJ, nascido no rio de janeiro em 1998. Atualmente mestrando em artes visuais no PPGAV, graduado em Pintura na Escola de Belas Artes &#45; UFRJ. Esta pesquisa se volta para investiga&#231;&#227;o da fronteira borrada entre o jogo das imagens t&#233;cnicas e o campo da pintura. A maneira como pensa o processo desses trabalhos &#233; uma investiga&#231;&#227;o das possibilidades de pintura, &#233; se colocar em jogo a todo o momento, brincando com a ideia de pintura e se apropriando de diversos materiais. Ryan experimenta o campo de cor RGB, explora as possibilidades de suporte e brinca com a ideia de simulacro.
texto-descricao: Estamos cotidianamente condicionados, por inúmeros fatores, ao contato e em certo sentido, ao consumo, de uma infinidade de “imagens” que se proliferam numa intensidade muito diferente daquela que nossos “olhos”, enquanto dispositivos ópticos de apreens&#227;o do sens&#237;vel, Podem ou desejam  compreender. Fomos historicamente ensinados, no interior de uma lógica predatória do vis&#237;vel, a tentar entender e, mais do que isso, capturar por meio do “olhar”, vilanizado e viciado, tudo aquilo que se apresentasse diante de nós. Enquanto desejo, essa ânsia ensimesmada em conhecer e apreender  tudo aquilo que uma “imagem” supostamente pretende informar, nos faz deslizar e sedimentar um caminho onde a certeza parece ser a única condi&#231;&#227;o  poss&#237;vel para a leitura do mundo contemporâneo. Por&#233;m, ao realizar tal opera&#231;&#227;o, onde o “olhar” sintetiza essas “certezas” em um ponto fixo capaz de  formalizar um “conhecimento” definitivo sobre as imagens, somos levados a um ponto extremo, onde automaticamente somos destitu&#237;dos desse “poder”  de aferir sobre a indubitabilidade do que vemos e nos defrontamos.
ano: 2022
tecnica: arte digital &#47; pintura
---

<img src="/assets/obras/VOLTAR.jpeg" alt="VOLTAR AOS PASSOS QUE FORAM DADOS" class="img-fluid d-block">
<br>`594x841 mm`
