---
layout: obra
title: MARIANA BRUMADINHO RIO ACIMA
thumbnail: /assets/thumbnail/t-MARIANA.png
artista: Triz de Oliveira Paiva
l-bio:  www.jardimdebeatriz.com.br
t-bio: Betraiz de Oliveira Paiva &#40;1984&#41; &#233; artista visual brasiliense e, por forma&#231;&#227;o, jornalista. Dedica&#45;se a pesquisar o jardim, adentrando suas profundezas. Entre outros, trajetória inclui&#58; Em 2023&#58; Pele Barro Pedra Grafite Rio, Individual simultanea na Galeria Rubem Valentim, no Espa&#231;o Cultural Renato Russo. Resid&#234;ncia art&#237;stica no Vilarejo 21, com acompanhamento cr&#237;tico com Mar&#237;lia Panitz e exposi&#231;&#227;o da Decurators. Mostra coletiva ObraBarro, no Museu das Bandeiras &#45; GO, na Cidade de Goiás, com curadoria de Suyan de Mattos, expografia e texto de Cris Cabus &#40;2023&#41;.  Em 2022&#58;  Resid&#234;ncia art&#237;stica Vilarejo 21 com curadoria cr&#237;tica de Christus Nóbrega e exposi&#231;&#227;o coletiva DevOrar a Antropofagia, na galeria Alfinete. Web&#45;exposi&#231;&#227;o coletiva &#34;Regime do Fogo&#34;, uma realiza&#231;&#227;o do media&#45;lab&#47;UnB com curadoria de Luciany Osório e Artur Cabral. Exposi&#231;&#227;o Itinerante em Casa, individual simultânea, no Casapark, com curadoria de Gracilene Bessa.
texto-descricao: É a partir de uma caminhada pelo jardim que trago quest&#245;es contemporâneas para o campo expositivo, tendo o caminhar como uma filosofia de ateli&#234;, para que a obra n&#227;o tenha o cheiro de poeira dos livros que ainda ecoam. Atravessando o rio de um lado para o outro, a coleta. O encontro com a mat&#233;ria &#233; importante nos processos, gera um certo tipo de entusiasmo para as m&#227;os que colaboram. A partir da provoca&#231;&#227;o do edital Rebrota,  sa&#237; para uma longa caminhada pelo jardim, subi a bacia do S&#227;o Francisco, onde bebe o Cerrado de Minas Gerais, adentrei as profundezas do Rio Doce. Tantas vidas enlamadas pela ganância de poucos. Tantos sonhos interrompidos resistem&#47;insistem em rebrotar sem sair do lugar. E há ainda mais de 400 minas em estado de risco, uma das mais cr&#237;ticas no munic&#237;pio de Rio Acima.  Na instala&#231;&#227;o MARIANA BRUMADINHO RIO ACIMA, a um só tempo, lirismo e denúncia.  Os galhos de barro giram como a bomba relógio de mais uma trag&#233;dia anunciada.
ano: 2023
tecnica: Video instala&#231;&#227;o
---

<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="/assets/videos/MARIANA.mp4"></iframe>
</div>
<br><br>


  <img src="/assets/obras/MARIANA.jpeg" alt="O corpo é bom para pensar" class="img-fluid d-block">
  `MARIANA BRUMADINHO RIO ACIMA`<br>  <br>
  

