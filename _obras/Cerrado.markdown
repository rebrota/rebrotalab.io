---
layout: obra
title: Cerrado SentioniX
thumbnail: /assets/thumbnail/t-Cerrado.png
artista: euFraktus_X
l-bio: linktr.ee&#47;eufraktus
t-bio: Criador e improvisador performático, nascido na Bahia, residente em Bras&#237;lia,  graduado em Música &#40;bacharel&#41;, mestre em Comunica&#231;&#227;o, especialista em Filosofia e Semiótica, doutor em Arte Contemporânea, pós&#45;graduado em Machine Learning e Intelig&#234;ncia Artificial. É pesquisador, conferencista e compositor de trabalhos experimentais e de trilhas musicais interativas para Teatro, Dan&#231;a, V&#237;deo e Performance desde os anos 1980, tendo se apresentado em mais de 20 pa&#237;ses na África, Am&#233;rica, Ásia e Europa. Regente da BSBLOrk&#45;Orquestra de Laptops de Bras&#237;lia, fundada em 2012,  foi aluno de composi&#231;&#227;o musical de H.&#45;J. Koellreutter e de Conrado Silva, toca guitarra no trio Gárgula, c&#237;tara indiana no trio Bioborgs, live electronics no Duo FraXtar &#40;Stellatum_&#41;, Duo 42 &#40;Julius Fujak&#41;, Duo JandriX &#40;Marika Morselli&#41; e &#233; conhecido como DJ euFraktus na cena do Trance e Deep House. Autor de livros e artigos em Música, Est&#233;tica e Semiótica. Desenvolvedor de software para interatividade digital, música fractal e Intelig&#234;ncia Artificialmente Aumentada em Max&#47;MSP.
texto-descricao: S&#233;rie de improvisos holofractais interativos de euFraktus_X &#40;aka Eufrasio Prates&#41; baseados em sons do Cerrado brasileiro.
ano: 2021&#45;2023
tecnica: Performance
---
<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="/assets/videos/Cerrado.mp4"></iframe>
</div>
 <br><br> `Playlist completa em`<br>     `https://youtube.com/playlist?list=PLRnJyuIKhdkaEOtVM5WGVDemO5SMaboxr&si=dBbelmMjo-OMyG8K`
