---
layout: obra
title: Etnomapa Demarcatório
thumbnail: /assets/thumbnail/t-Etonomapa.png
artista: Wesley Lima Brito
l-bio: https://linktr.ee/wesleylimabrito
t-bio: Wesley Lima Brito, nascido e criado em São Paulo, formado no curso Técnico em Redes de Computadores (CEAP) e Bacharel em Artes Visuais pelo Centro Universitário Belas Artes de São Paulo (FEBASP). O artista possui trabalhos publicados em revistas e artigos, já ocupou espaços internacionais e nacionais, dentre eles&#58;   Ateliê397, Eixo Arte, Bienal Oswaldo Goeldi, Salão Nacional de Arte Contemporânea de Guarulhos, Espaço Cultural Correios Niterói e Colabirinto.<br> Sua linha de pesquisa se desdobra em três áreas&#58;  Arte sonora &#45; Através de experimentações acerca de seu cotidiano, gravações e paisagens sonoras, intervenções, instalações, performances e obras audiovisuais. Cultura Indígena - abordando aspectos culturais indígenas por meio de releituras e apropriações. Cidade - incorporando a rotina, movimento, tempo, deterioração de espaços, pixações e contrastes sociais.
texto-descricao: Como transpor uma grafia indígena, com todo o seu significado, para o espaço tridimensional podendo ser alterada e construída a partir de novas formas? <br> Na obra Etnomapa Demarcatório, foram elaboradas peças de cerâmica, argila em seu ponto de coro (onde perde-se a plasticidade do material), ambas com suas particularidades de pigmentação mantidas e outras sofreram a intervenção de cor, com tinta acrílica. Optou-se por pintar algumas peças com tinta, para representar as cores dos grafismos e suas características, que originalmente são pigmentadas com materiais orgânicos, no trabalho Etnomapa Demarcatório esta representação deu-se por manter algumas peças em sua pigmentação natural da terra e outras pela pigmentação adquirida após o processo de queima em alta temperatura. <br> É importante levantar as questões de representatividade a respeito da cultura indígena, sobre o apagamento diante de nosso governo atual, que se coloca distante em relação a dignidade humana , cuidados, direitos, responsabilidades e demarcações. <br> Diante, deste problema e temática surge a ideia da realização do trabalho; a partir de pesquisas, referências e do estudo de grafismos desenvolvidos pelas mais diversas tribos indígenas brasileiras, realizo uma releitura de grafismos indígenas realizados pela tribo Asurini (Tocantins). <br> Como resultado, obtenho diversas peças, de variados tamanhos e padrões, as peças são organizadas e dispostas baseadas dentro de uma média de habitantes e demarcações existentes no Brasil (2018). Os núcleos de peças coloridas concentram os territórios indígenas que aguardam demarcação há mais de 20 a 30 anos.
ano: 2019
tecnica: Escultura
---

  <div class="menu d-none d-sm-block">


  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
     


   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/Etnomapa/1.jpeg" alt="Etnomapa Demarcatório" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Etnomapa/2.jpeg" alt="Etnomapa Demarcatório" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Etnomapa/3.jpeg" alt="Etnomapa Demarcatório" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Etnomapa/4.jpeg" alt="Etnomapa Demarcatório" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Etnomapa/5.jpeg" alt="Etnomapa Demarcatório" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Etnomapa/6.jpeg" alt="Etnomapa Demarcatório" class="img-fluid mx-auto d-block">
      </div>
  </div>
 
 
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>





<div class="d-block d-sm-none">
  
  <img src="/assets/obras/Etnomapa/1.jpeg" alt="Etnomapa Demarcatório" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Etnomapa/2.jpeg" alt="Etnomapa Demarcatório" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Etnomapa/3.jpeg" alt="Etnomapa Demarcatório" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Etnomapa/4.jpeg" alt="Etnomapa Demarcatório" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Etnomapa/5.jpeg" alt="Etnomapa Demarcatório" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Etnomapa/6.jpeg" alt="Etnomapa Demarcatório" class="img-fluid" width="35%">
  
</div>

  
