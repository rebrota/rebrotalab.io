---
layout: obra
title: Jardim de cinzas
thumbnail: /assets/thumbnail/t-Jardim.png
artista: Ana Lúcia Canetti
l-bio: www.analuciacanetti.com.br
t-bio: Artista visual, doutoranda em Artes Visuais na Universidade de Bras&#237;lia &#40;UnB&#41;, licenciada em Artes Visuais pela Universidade Estadual do Paraná – Campus de Curitiba II – Faculdade de Artes do Paraná &#40;2007&#41;, mestre em Psicologia pela Universidade Federal de Santa Catarina &#40;2010&#41;, linha de pesquisa Relações éticas, estéticas e processos de criação e psicóloga pela Universidade Federal do Paraná &#40;2004&#41;. Atualmente trabalha com a&#231;&#245;es de colheitas de cinzas em áreas incendiadas do cerrado brasileiro e com uma pesquisa po&#233;tica na cerâmica sobre as transforma&#231;&#245;es do fogo, da terra e das mat&#233;rias vegetais.
texto-descricao: O trabalho prop&#245;e a intera&#231;&#227;o do espectador com uma imagem fotográfica de brotos verdes que nascem do solo queimado de uma área do Parque Nacional de Bras&#237;lia. Parte&#45;se da refer&#234;ncia de objetos conhecidos como jardins zens ou jardins japoneses, onde o participante manipula areia e pedras em uma caixa, promovendo marcas e desenhos. No caso da obra apresentada, o espectador vai produzindo um jogo de encobrir e descobrir brotos verdes, a partir do breu das cinzas do cerrado. Este bioma vive atualmente grave amea&#231;a devido ao excesso de queimadas e desmatamento. Contudo, tem no desenvolvimento de suas rebrotas, uma rela&#231;&#227;o importante com o  fogo e seus regimes naturais o controlados. 
ano: 2023
tecnica: Escultura interativa
---

<img src="/assets/obras/Jardim.jpeg" alt="Jardim de cinzas" class="img-fluid d-block">
<br>`200x300x50 mm`
