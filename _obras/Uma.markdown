---
layout: obra
title: Uma noite no serrado
thumbnail: /assets/thumbnail/t-Uma.png
artista: joseMar blures
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;estrela_ao_mar
t-bio: 1987, soteropolitano. &#40;1999&#41;, muda&#45;se para Alagoinhas&#45;BA. &#40;2004&#41;, inicia sua trajetória art&#237;stica pela influ&#234;ncia de movimentos da cultura urbana, como&#58; Hip Hop, Skate e o Punk Rock, produzindo&#58; desenho, pixo, e graffiti. &#40;2007&#41;, participou do 2° Encontro Nordestino de Hip Hop em Jo&#227;o Pessoa &#45; PB. &#40;2008&#41;, inicia gradua&#231;&#227;o em Fisioterapia. &#40;2010&#41;, realiza primeira exposi&#231;&#227;o individual no Centro de Cultura de Alagoinhas &#45; BA. &#40;2013&#41;, inicia o mestrado no Programa de Pós&#45;gradua&#231;&#227;o em Desenho, Cultura e Interatividade &#45; PPGDCI &#45; UEFS. &#40;2014&#41;, modifica sua po&#233;tica e linguagem para ampliar sua express&#227;o, partindo para&#58; fotografia, pintura, objeto, instala&#231;&#227;o, oficinas de arte&#45;educa&#231;&#227;o e exposi&#231;&#245;es. &#40;2015&#41;, participa do 3° Congresso transdisciplinar est&#233;ticas de rua &#45; ENAH, Cidade do M&#233;xico &#45; ME. &#40;2016&#41;, leciona Anatomia Humana no ensino superior em cursos da área da saúde, inserindo sua pesquisa desenvolvida no Mestrado enquanto metodologia de ensino e aprendizagem, ao passo que exerce o fazer de artista e fisioterapeuta. &#40;2017&#41;, participa de exposi&#231;&#227;o coletiva na Galeria Meia Lua, na Escola Nacional de Antropologia e História &#45; ENAH, Cidade do M&#233;xico &#45; ME. &#40;2022&#41;, participou da exposi&#231;&#227;o virtual Ars Sexualis &#47; Seminário de artes visuais&#58; Sexualidades dissidentes do Sul Global. PPGARTS &#45; UERJ. &#40;2022&#41;, publicou sua disserta&#231;&#227;o em formato de livro. Desenho e Fisioterapia&#58; a contribui&#231;&#227;o do exerc&#237;cio do desenho para o aprendizado da cinesiologia, Editora Oxente. &#40;2023&#41; selecionado para a exposi&#231;&#227;o de artes visuais, Al&#233;m da Parada, na Galeria Objetos do Olharaluno, S&#227;o Paulo &#45; SP. &#40;2023&#41; doutorando no Programa de Pós Gradua&#231;&#227;o em Artes Visuais &#45; PPGAV &#47; UFBA.
texto-descricao: Poeira estelar formam nossos corpos. C&#233;us no serrado &#233; quintal de artista, memórias sociais e particulares guardam e entretecem imagens distintas. Planetas, sat&#233;lites e constela&#231;&#245;es inteiras, formadas a partir de uma heran&#231;a cultural que sobrevive em sil&#234;ncio, nos cantos da casa, em baixo do p&#233; de ip&#234;, na reza de Santo Antônio. De um passado &#237;ntimo ao espa&#231;o onde agora flutuam, &#233; preciso olhar para o c&#233;u em noites escuras, ver s&#237;mbolos luminosos de modos de vida e arte, escaparem por fendas do tempo&#47;espa&#231;o cósmico e terreno, de lembran&#231;a ou de afeto. Estrelas cadentes concedem desejos, fa&#231;a o seu!
ano: 2023
tecnica: Porcelana 
---

<div class="menu d-none d-sm-block">


  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>
      <li data-target="#imagens" data-slide-to="7"></li>
      <li data-target="#imagens" data-slide-to="8"></li>
      <li data-target="#imagens" data-slide-to="9"></li>
      <li data-target="#imagens" data-slide-to="10"></li>
      <li data-target="#imagens" data-slide-to="11"></li>
      <li data-target="#imagens" data-slide-to="12"></li>
      <li data-target="#imagens" data-slide-to="13"></li>
      <li data-target="#imagens" data-slide-to="14"></li>
      <li data-target="#imagens" data-slide-to="15"></li>
      <li data-target="#imagens" data-slide-to="16"></li>
      <li data-target="#imagens" data-slide-to="17"></li>
      <li data-target="#imagens" data-slide-to="18"></li>
      <li data-target="#imagens" data-slide-to="19"></li>
      <li data-target="#imagens" data-slide-to="20"></li>
      <li data-target="#imagens" data-slide-to="21"></li>
      <li data-target="#imagens" data-slide-to="22"></li>
      <li data-target="#imagens" data-slide-to="23"></li>
      <li data-target="#imagens" data-slide-to="24"></li>
      <li data-target="#imagens" data-slide-to="25"></li>
      <li data-target="#imagens" data-slide-to="26"></li>

     


   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/Uma/1.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Uma/2.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Uma/3.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/Uma/4.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
            <div class="carousel-item">
        <img src="/assets/obras/Uma/5.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
            <div class="carousel-item">
        <img src="/assets/obras/Uma/6.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/7.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/8.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/9.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/10.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/11.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/12.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/13.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/14.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
		<div class="carousel-item">
        <img src="/assets/obras/Uma/15.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/16.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/17.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/18.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/19.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
      <img src="/assets/obras/Uma/20.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/21.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/22.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/23.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/24.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
		<div class="carousel-item">
        <img src="/assets/obras/Uma/25.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
        <div class="carousel-item">
        <img src="/assets/obras/Uma/26.jpg" alt="Uma noite no serrado" class="img-fluid mx-auto d-block">
      </div>
  </div>
 
 
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>





<div class="d-block d-sm-none">
  
  <img src="/assets/obras/Uma/1.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Uma/2.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Uma/3.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Uma/4.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Uma/5.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Uma/6.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Uma/7.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Uma/8.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Uma/9.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
     <img src="/assets/obras/Uma/10.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Uma/11.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Uma/12.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Uma/13.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Uma/14.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Uma/15.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Uma/16.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Uma/17.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Uma/18.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Uma/19.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
 <img src="/assets/obras/Uma/20.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Uma/21.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Uma/22.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/Uma/23.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/Uma/24.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Uma/25.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%"><br><br>
   <img src="/assets/obras/Uma/26.jpg" alt="Uma noite no serrado" class="img-fluid" width="35%">
  
</div>