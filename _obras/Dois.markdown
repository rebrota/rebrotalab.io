---
layout: obra
title: Dois minutos de banho de Sol
thumbnail: /assets/thumbnail/t-Dois.png
artista: Luciana Arslan e Paulo Augusto (coletivo lupa)
l-bio: https://www.instagram.com/luciana.arslan
t-bio: Luciana Arslan, transita entre as áreas de ensino, dança e performance. Suas práticas artísticas e pesquisas abordam as relações entre corpo, cognição e experiência estética. É autora dos livros&#58;Corpo (sentido)&#58; corporeidade e estesia nos processos de ensino-aprendizagem, e  do livrodança "Danças para vestir o pensamento", entre outros. <br>Paulo Augusto é fotografo.  Doutor em Arte e Cultura Visual ; mestre em História Social ;  licenciado em Artes Visuais, atualmente trabalha como  Gestor Cultural na Secretaria Municipal de Cultura- Prefeitura Municipal de Uberlândia  onde realiza  curadorias em Cinema no Cineclube Cultura  e do Papo Fotográfico. Também está realizando pósdoc no Programa de PósGraduação de Arquitetura e Urbanismo da UFU. 
texto-descricao: Dois minutos de banho de Sol (2022) é um trabalho do coletivo LUPA ( Luciana Arslan e Paulo Soares Augusto ). As relações entre corpo e território, tempo de trabalho e tempo de descanso, de uma relação mais cosmogônica com o tempo são algumas das questões que nos mobilizaram nessa série composta de vídeo e foto performances que surgiu quando estávamos vivendo em Araguari, no  Cerrado (que coincidentemente significa fechado em espanhol).
ano: 2022
tecnica: videoperformance 
---
<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="/assets/videos/Dois.mp4"></iframe>
</div>
  `Dois minutos de banho de Sol`<br><br>
