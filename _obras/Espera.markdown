---
layout: obra
title: Espera e acorda
thumbnail: /assets/thumbnail/t-Espera.png
artista: Balduino 
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;guiibalduino
t-bio: Sou um ilustrador que busca atrav&#233;s minha vis&#227;o do mundo a reinterpreta&#231;&#227;o da realidade apresentando o resultado da soma das refer&#234;ncias em minha volta e a reconstru&#231;&#227;o em minha mente de uma nova vis&#227;o minha e do todo. Objeto principal  para isso &#233; a trav&#233;s das canetas a base de nanquim que me ajudam a realizar um jogo de transformas as cores das refer&#234;ncia em um jogo entre o branco e o preto &#40;papel e nanquim&#41;.
texto-descricao: Assim como o rebrotar do cerrado, que é esperar o momento das primeiras chuvas para renascer, os desenhos trazem uma espera para renascer e se iluminar com força para existir, em um círculo de vida em mutação, representando a persistência por meio das plantas, objetos e animais.
ano: 2023
tecnica: Desenho 
---

<img src="/assets/obras/Espera/1.jpeg" alt="Seres Retirantes" class="img-fluid d-block">
<br><br>
<img src="/assets/obras/Espera/2.jpeg" alt="Seres Retirantes" class="img-fluid d-block">
<br><br>