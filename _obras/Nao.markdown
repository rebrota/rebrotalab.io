---
layout: obra
title: Não há lugar como nosso lar 
thumbnail: /assets/thumbnail/t-Nao.png
artista: Marta Mencarini
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;marta.mencarini.art
t-bio: Marta Mencarini Guimar&#227;es nasceu, reside e trabalha em Bras&#237;lia &#45; DF. Doutoranda em Arte pela Universidade de Bras&#237;lia UnB, na linha Po&#233;ticas Transversais, sob orienta&#231;&#227;o Dra. Bia Medeiros e co&#45;orienta&#231;&#227;o da Dra. Silvana Macedo &#40;UDESC&#41;. Artista visual, professora, pesquisadora e m&#227;e, desenvolve pesquisas sobre feminismos e maternagem na arte contemporânea, abordando a maternidade enquanto experi&#234;ncia potencial e pol&#237;tica. Investiga a po&#233;tica art&#237;stica em auto narrativas e auto fic&#231;&#245;es e escrita de si, utilizando principalmente da pintura, fotografia e escrita.  Tem experi&#234;ncia na área de artes, transitando pela história da arte, pintura, fotografia, performance, interven&#231;&#227;o urbana, arte tecnologia e v&#237;deo. Co &#45; coordena a coletiva e mapeamento Arte e Maternagem &#40;AeM&#41;, integrante do Coletivo Matriz e do Grupo Mesa de Luz.
texto-descricao:  “N&#227;o há lugar como nosso lar” &#40;2021&#41; óleo sobre tela, 54x65 cm. Trava um diálogo estreito com o conto “O mágico de Oz”, trafegando sob a mistura entre realidade e fic&#231;&#227;o, de inventos lúdicos&#45;devaneios&#45;sonhos infanto&#45;juvenis projetados à vida adulta. Buscando abordar as ambival&#234;ncias entre sonho e realidade. Em uma paisagem do quintal de plantas nativas do cerrado, vegetais e árvores frut&#237;feras, envolvem a paisagem lúdica das brincadeiras infantis e em quais sentidos s&#227;o performados cada um dos personagens do conto na busca por uma ing&#234;nua&#45;romântica ideia de perfei&#231;&#227;o do lar&#45;familiar.
ano: 2021
tecnica: Pintura 
---

<img src="/assets/obras/Nao.jpeg" alt="Não há lugar como nosso lar" class="img-fluid d-block">
<br>`540x650 mm`
