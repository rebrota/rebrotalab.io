---
layout: obra
title: PLANTUMANA
thumbnail: /assets/thumbnail/t-PLANTUMANA.png
artista: FEBA
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;felipebarrosarruda
t-bio: Felipe Barros Arruda, FEBA. Afroind&#237;gena natural de Goiânia&#45;GO &#45; 1997 &#45; Reside em Botucatu desde os 4 anos.  É um premiado bioartista, performer, educador, poeta, paisagista, designer floral, skatista e empreendedor socioambiental, destacando em sua produ&#231;&#227;o po&#233;tica a cria&#231;&#227;o de obras naturais integrais e regenerativas com elementos da natureza nativa de Cerrado, coletados por meio de bioexpedi&#231;&#245;es.  Sua obra de maior circula&#231;&#227;o doi o projeto de ecoperformance Plantumana, a qual foi exposta na Pinacoteca Fórum das Artes durante exposi&#231;&#227;o &#58; Afromodernos e publicada no Fórum Fashion Revolution Brasil. Em parceria com o coletivo Flanantes, a v&#237;deoperformance foi transmitida no Paris Surf & Skateboard Film Festival,  Paris &#45; Fran&#231;a e exibida no Festival Serrinha, durante resid&#234;ncia art&#237;stica a convite do artista Ben&#233; Fonteles. Em 2022, foi publicada na revista de cultura cient&#237;fica Climacon, Unicamp. No mesmo ano foi eleito pela ONU Brasil representante da sociedade civil na Virada dos Objetivos do Desenvolvimento Sustentável, no Parque do Ibirapuera. 

texto-descricao: O projeto Plantumana criado pelo bioartista, ativista e educador Felipe Barros Arruda “FEBA”, prop&#245;e desenvolver uma pesquisa po&#233;tica e regional entre uma arte natural e regenerativa, que conecta performance, ecologia, educa&#231;&#227;o e mudan&#231;as climáticas; a partir da realiza&#231;&#227;o dessa obra&#45;vestivel ativada por meio de uma ecoperformance na paisagem, visando depois seu desfazimento e reintegra&#231;&#227;o ao solo. Esta ecoperformance prop&#245;e narrativas ecoperformáticas que ir&#227;o abordar histórias ecológicas, ancestrais e territoriais a partir de uma perspectiva decolonial, afroind&#237;gena e do Sul Global.  Os conceitos que orientam o projeto Plantumana tem como propósito gerar uma reflex&#227;o conceitual, est&#233;tica e catal&#237;tica que visa a estrutura&#231;&#227;o de uma ecoperformance por meio de roupas&#45;vestimentas feitas com materiais naturais e urbanos. Produzida por meio de uma pesquisa que atravessa a arte, performance, antropologia, educa&#231;&#227;o, moda, mudan&#231;as climáticas, ecologia, atrav&#233;s de uma prática performática de imaginar e criar novas formas de ser, viver, habitar e construir rela&#231;&#245;es multiesp&#233;cies entre humanos e n&#227;o&#45;humanos, no contexto do Antropoceno e do Sul Global. O projeto Plantumana tem uma abordagem de pesquisa po&#233;tica, cr&#237;tica e ambiental sobre o conhecimento popular e a cultura afroind&#237;gena, a indústria da moda, o racismo ambiental e as mudan&#231;as climáticas. Tais interesses ser&#227;o elaborados por meio da cria&#231;&#227;o e produ&#231;&#227;o de uma s&#233;rie de a&#231;&#245;es art&#237;sticas, culturais, educacionais e formativas, que ser&#227;o viabilizadas atrav&#233;s da constru&#231;&#227;o de uma narrativa com um diálogo transversal, integral e multidisciplinar entre os saberes ancestrais, populares e o conhecimento cient&#237;fico com importantes quest&#245;es relacionadas a arte e ecologia contemporâneas, como intera&#231;&#227;o do público com a obra, diálogo interesp&#233;cies, mudan&#231;as climáticas e Antropoceno. As proposi&#231;&#245;es que esta obra irá debater com o público, tem aporte e consonância com grandes pensadores da história e da contemporaneidade como Donna Haraway, Bruno Latour, Ailton Krenak, Isabelle Stengers, Deborah Danowski e Eduardo Viveiros de Castro, Francesco Careri, dialogando com importantes artistas como Ben&#233; Fonteles, N&#233;le Azevedo, Jaider Esbell, Eduardo Kac, Gustavo Caboco, Daniel Caballero, Uyra, Denilson Baniwa, Maxwell Alexandre, H&#233;lio Oiticica e Bispo do Rosário. Dentre esses artistas se destacam as importantes refer&#234;ncias no processo de pesquisa e pensamento da obra, como com H&#233;lio Oiticica, por meio das obras Parangol&#233;s &#40; 1960 &#41;, Bispo do Rosário com o Manto de Apresenta&#231;&#227;o do Ju&#237;zo Final &#40; sem data &#41;. Inspirado nas expedi&#231;&#245;es do fotógrafo ecologista ativista Hans Wolfgang Silvester &#40; 1938 &#45; &#41; eem retomada da sua ancestralidade como afro&#237;ndigena, FEBA, se baseou nos testemunhos fotográficos dos povos do Vale do Omo, para utilizar os adornos naturais, costume t&#237;pico da tribos Mursi e Surma, do Sul da Etiópia, como refer&#234;ncias para compor a Plantumana, considerando o seu processo de vestir como parte integrante da natureza em forma de um ritual sagrado, est&#233;tico e cotidiano para as tribos. Como parte do processo, FEBA busca fazer uma rela&#231;&#227;o po&#233;tica e pol&#237;tica que busca trazer em evid&#234;ncia a situa&#231;&#227;o de apagamento e exterm&#237;nio da cultura ind&#237;gena na regi&#227;o de Botucatu em um processo de retomada atrav&#233;s do diálogo e visibilidade para a cultura e situa&#231;&#227;o das tribos Mursi e Surma e seus mais recentes impactos decorrentes da constru&#231;&#227;o da maior represa hidrel&#233;trica da África em seu território. A cultura milenar destes ind&#237;genas encontra&#45;se atualmente em grave perigo já que est&#227;o sendo obrigados a renunciar, sem nenhum tipo de compensa&#231;&#227;o, a suas terras no Parque Nacional do Omo, por servidores públicos do Governo segundo denúncia da ONG &#34;Native Solutions to onservation Refugees&#34;
ano: 2021
tecnica: Ecoperformance
---

<div class="menu d-none d-sm-block">


  <div id="imagens" class="carousel slide" data-ride="carousel">


   <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
     


   </ul>
    
   <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/PLANTUMANA/1.jpeg" alt="PLANTUMANA" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/PLANTUMANA/2.jpeg" alt="PLANTUMANA" class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/PLANTUMANA/3.jpeg" alt="PLANTUMANA" class="img-fluid mx-auto d-block">
      </div>
  </div>
 
 
   <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
<br>





<div class="d-block d-sm-none">
  
  <img src="/assets/obras/PLANTUMANA/1.jpeg" alt="PLANTUMANA" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/PLANTUMANA/2.jpeg" alt="PLANTUMANA" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/PLANTUMANA/3.jpeg" alt="PLANTUMANA" class="img-fluid" width="35%"><br><br>
  
</div>