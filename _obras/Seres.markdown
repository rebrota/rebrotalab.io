---
layout: obra
title: Seres Retirantes 
thumbnail: /assets/thumbnail/t-Seres.png
artista: Jurema
l-bio: https&#58;&#47;&#47;www.instagram.com&#47;juremagrita
t-bio: Jurema indignada com o tratamento dos humanos com sua natureza, reivindica aten&#231;&#227;o de todos ao descaso com a terra e as vidas que dependem dela. 
texto-descricao: Seres Retirantes, releitura de &#34;Os Retirantes&#34;, obra do pintor Candido Portinari de 1944, continua retratando a retirarem dos seres de sua terra por uma condi&#231;&#227;o melhor e digna de vida. No entanto a fuga agora n&#227;o &#233; apenas devido à seca, mas sim devido ao desmantelo e desmatamento do Cerrado, e a falta de condi&#231;&#245;es de vida para os seres que ali re&#45;existem. Seres Retirantes &#233; uma revolta, &#233; um protesto, &#233; um pedido de socorro da natureza que clama acima de tudo por consci&#234;ncia.
ano: 2022
tecnica: Pintura
---

<img src="/assets/obras/Seres.jpeg" alt="Seres Retirantes" class="img-fluid d-block">
<br>`600x900 mm`