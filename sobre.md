---
title: Sobre
layout: default
---

<div class="container-fluid">
  <div class="row">
    <div class="sobre col-sm-10">
      <h5>
De uma resiliência quase transcendental, o Cerrado rebrota. Da devastação insana, o bioma resiste, insistindo em nos mostrar que ainda é tempo de vida, que apesar de não ouvirmos suas súplicas obstinadas, que ainda há chance para nós. <br>
Os últimos anos desnudaram a teimosia humana em negar o óbvio, em postergar o urgente para um tempo que já não temos mais. O cansaço e o pessimismo que nos provocou constantemente, agora dá lugar ao esperançar de Freire e ao ímpeto do aqui e agora. Essa pressa vem da convicção de que já não é mais possível dar um passo para trás para poder avançar dois, e sim, de que não cabe mais caminhar, é preciso correr. <br>
Apesar de afoitos sabemos que esta corrida não deverá ser trilhada às cegas, pois a ciência nos guiará com seus fatos e dados, os povos originários nos presentearão com sabedoria e boas práticas e a arte não nos deixará esmorecer e nem esquecer o compromisso inabalável com a sobrevivência. É hora de selarmos um pacto com a terra, com a água, com a semente, com as vidas, todas as vidas. É hora de compreender que a existência humana não se sobrepõe a existência de qualquer outra espécie nesse planeta. Que abusos não serão perdoados, que consciências não serão abrandadas, que o pacto é ao mesmo tempo coletivo e individual.<br>
A web-exposição REBROTA e seus 37 artistas participantes, de diversas regiões, origens, raças, etnias e credos, se comprometem através da arte de não compactuarem com o silêncio e a inércia diante da destruição e exploração dos nossos biomas, de nossa gente, de nossa cultura, de nossas vidas. Através de obras que discutem e repercutem o Cerrado brasileiro somos instigados a pensar e sentir além dos territórios e corpos que os habitam, somos convidados a olhar para dentro e para fora de nossas cascas secas e rugosas. <br>
Que o constante mover-se da natureza nos inspire a criar, recriar e rebrotar.<br>
      </h5>
      <p class='nome-autor'></p>
    </div>
  </div>
</div>
